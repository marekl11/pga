# PGA

## git LFS
Toto repo pracuje s git LFS (Large File Storage). Jedná se o rozšíření které efektivněji pracuje s velkými soubory jako je grafika, díky čemuž je projekt na lokálním disku menší
a práce s ním je responzivnější. Soubory spravované pomocí git LFS jsou k nalezení v .gitattributes. Pro běžného uživatele se workflow nemění a veškerá magie se děje pod kapotou.
Stále platí add-commit-push-pull, jen je potřeba mít toto rozšíření nainstalované, nic víc. 

Pro bezproblémový chod: `sudo apt-get install git-lfs`

více zde: https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/