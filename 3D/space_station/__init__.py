bl_info = {
    "name": "Space station room generator",
    "description": "Parametrically generates a rectangular space station room",
    "author": "Lukas Marek",
    "version": (1, 0, 0),
    "blender": (2, 93, 6),
    "location": "View3D > Add > Mesh > Space station",
    "warning": "",
    "wiki_url": "",
    "category": "Add Mesh"
}

import bpy
from mathutils import *
from math import *
from bpy.props import *
import os
import math
import random

def relativePath(file):
    dirname = os.path.dirname(__file__)
    return os.path.join(dirname, file)

class CspaceStation(bpy.types.Operator):
    """Space station"""
    bl_idname = "object.space_station"
    bl_label = "Space station"
    bl_options = {'REGISTER', 'UNDO'}
    
    lenght : bpy.props.IntProperty(
        name = 'Lenght', 
        description = 'Number of tiles',
        default = 1, min = 1, max = 10, step = 1
    )

    width : bpy.props.IntProperty(
        name = 'Width', 
        description = 'Number of tiles',
        default = 1, min = 1, max = 10, step = 1
    )

    doorsNumber : bpy.props.IntProperty(
        name = 'Doors', 
        description = 'Number of doors',
        default = 0, min = 0, max = 40, step = 1
    )

    windowsNumber : bpy.props.IntProperty(
        name = 'Windows', 
        description = 'Number of windows',
        default = 0, min = 0, max = 20, step = 1
    )

    showCeiling : bpy.props.BoolProperty(
        name = 'Show ceiling', 
        description = 'Toggles generation of ceiling',
        default = False
    )
    
    seed : bpy.props.IntProperty(
        name = 'Seed', 
        description = 'Seed for random generation of rooms',
        default = 42
    )

    def execute(self, context):
        """main"""
        self.objects.clear()
        random.seed(self.seed)

        print(self.generateIndexes(3, 10))

        self.generateSurface()
        if self.showCeiling == True:
            self.generateSurface(True)

        leftSideDoors = random.randint(0, self.doorsNumber)
        if leftSideDoors > self.lenght * 2:
            leftSideDoors = self.lenght * 2
        rightSideDoors = self.doorsNumber - leftSideDoors
        if rightSideDoors > self.lenght * 2:
            rightSideDoors = self.lenght * 2
        leftSideDoors = self.doorsNumber - rightSideDoors

        self.generateLenghtWall(leftSideDoors)
        self.generateLenghtWall(rightSideDoors, True)

        backSideWindows = random.randint(0, self.windowsNumber)
        if backSideWindows > self.width:
            backSideWindows = self.width
        frontSidewindows = self.windowsNumber - backSideWindows
        if frontSidewindows > self.width:
            frontSidewindows = self.width
        backSideWindows = self.windowsNumber - frontSidewindows

        self.generateWidthWall(backSideWindows)
        self.generateWidthWall(frontSidewindows, True)

        self.offsetToCenter()

        return {'FINISHED'}

    objects = []

    def generateSurface(self, isCeiling = False):
        """generates floor and ceiling"""
        floor = self.importObject(relativePath("3D_models\\floor.fbx"))
        if isCeiling:
            floor.delta_rotation_euler = (math.radians(180), 0.0, 0.0)
        for l in range(0, self.lenght):
            for w in range(0, self.width):
                copiedFloor = self.copyObject(floor)
                copiedFloor.location.x = 2 * l
                copiedFloor.location.y = 1.5 * w
                if isCeiling:
                    copiedFloor.location.z = 2.5
        self.deleteObject(floor)

    def generateLenghtWall(self, numberOfDoorsToGenerate, isRight = False):
        """generates wall of big panels and doors"""
        doorsIndexes = self.generateIndexes(numberOfDoorsToGenerate, self.lenght * 2)
        door = self.importObject(relativePath("3D_models\\door.fbx"))
        door.location.x -= 0.5

        bigPanel = self.importObject(relativePath("3D_models\\bigPanel.fbx"))
        bigPanel.location.x -= 0.5
        
        if isRight:
            bigPanel.delta_rotation_euler = (0.0, 0.0, math.radians(180))
            bigPanel.location.y += 0.75
            door.delta_rotation_euler = (0.0, 0.0, math.radians(180))
            door.location.y += 0.75
        else:
            bigPanel.location.y -= 0.75
            door.location.y -= 0.75
        for l in range(0, self.lenght * 2):
            if l in doorsIndexes:
                copiedPiece = self.copyObject(door)
            else:
                copiedPiece = self.copyObject(bigPanel)
            copiedPiece.location.x += l
            if isRight:
                copiedPiece.location.y += (self.width - 1) * 1.5

        self.deleteObject(bigPanel)
        self.deleteObject(door)
    
    def generateWidthWall(self, numberOfWindowsToGenerate, isFront = False):
        """generates wall of small panels and windows"""
        windowsIndexes = self.generateIndexes(numberOfWindowsToGenerate, self.width)
        window = self.importObject(relativePath("3D_models\\window.fbx"))

        smallPanel = self.importObject(relativePath("3D_models\\smallPanel.fbx"))
        smallPanel.location.y -= 0.5
        if isFront:
            smallPanel.location.x += 1
            smallPanel.delta_rotation_euler = (0.0, 0.0, math.radians(90))
            window.delta_rotation_euler = (0.0, 0.0, math.radians(90))
            window.location.x += 1
        else:
            smallPanel.location.x -= 1
            smallPanel.delta_rotation_euler = (0.0, 0.0, math.radians(-90))
            window.delta_rotation_euler = (0.0, 0.0, math.radians(-90))
            window.location.x -= 1
        for w in range(0, self.width * 3):
            if w // 3 in windowsIndexes:
                pass
            else:
                copiedSmallPanel = self.copyObject(smallPanel)
                copiedSmallPanel.location.y += w * 0.5
                if isFront:
                    copiedSmallPanel.location.x += (self.lenght - 1) * 2

        for w in range(0, self.width):
            if w in windowsIndexes:
                copiedWindow = self.copyObject(window)
                copiedWindow.location.y += w * 1.5
                if isFront:
                    copiedWindow.location.x += (self.lenght - 1) * 2

        self.deleteObject(smallPanel)
        self.deleteObject(window)

    def offsetToCenter(self):
        """move all objects to center"""
        for object in self.objects:
            object.location.x -= (self.lenght * 2) / 2
            object.location.x += 2 / 2
            object.location.y -= (self.width * 1.5) / 2
            object.location.y += 1.5 / 2

    def generateIndexes(self, numberOfIndexes, maxIndex):
        """generates random indexes for positions of doors and windows"""
        indexes = list(range(0, maxIndex))
        random.shuffle(indexes)
        return indexes[0:numberOfIndexes]

    def importObject(self, path):
        """import fbx"""
        old_objects = set(bpy.context.scene.objects)
        bpy.ops.import_scene.fbx(filepath=path)
        imported_objects = set(bpy.context.scene.objects) - old_objects
        imported_obj = imported_objects.pop()
        
        return imported_obj

    def copyObject(self, object):
        """copy object"""
        copiedObject = object.copy()
        bpy.context.collection.objects.link(copiedObject)
        self.objects.append(copiedObject)

        return copiedObject

    def deleteObject(self, object):
        """delete object"""
        bpy.ops.object.select_all(action='DESELECT')
        object.select_set(True)
        bpy.ops.object.delete()


classes = [CspaceStation]    
    
    
def menu_func(self,context):
    self.layout.operator(CspaceStation.bl_idname)    
    
def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func)
    
def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class(cls)
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func)  
