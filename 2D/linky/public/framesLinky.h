#pragma once
#include <nlohmann/json.hpp>

//rounds float into an integer
int floatToInt(float x);

//retuns json of frames
nlohmann::json jsonFrames(GimpDrawable *drawable);