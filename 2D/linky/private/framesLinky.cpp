#include <iostream>
#include <vector>
#include <libgimp/gimp.h>
#include <nlohmann/json.hpp>

#include "framesLinky.h"

#include <chrono>

int floatToInt(float x)
{
    return (int)(x + 0.4999f);
}

nlohmann::json jsonFrames(GimpDrawable *drawable)
{
    //get layers
    gint32 image_ID = gimp_item_get_image(drawable->drawable_id);
    gint *num_layers = g_new(gint, 1);
    gint *layers = g_new(gint, 7920);
    layers = gimp_image_get_layers(image_ID, num_layers);
    std::cout << *num_layers << std::endl;

    //initialize progress bar
    gimp_progress_init("Creating Json of animation frames...");

    auto started = std::chrono::high_resolution_clock::now();

    const int FRAMES = *num_layers;
    const int COLUMNS = 5;
    const int ROWS = 204;
    const int RGB = drawable->bpp;

    //get image size
    int imageSize = drawable->height;

    //4D vector
    std::vector<std::vector<std::vector<std::vector<guint8>>>> vectorFrames(FRAMES, std::vector<std::vector<std::vector<guint8>>>(COLUMNS, std::vector<std::vector<guint8>>(ROWS, std::vector<guint8>(RGB))));

    //allocate collumn
    guint8 *collumn = g_new(guint8, imageSize * RGB);

    //space between columns and rows defined as floats, this prevents integer rounding which causes pixel loss in last rows/colums
    float columnStep = (float)(imageSize-1) / (float)(COLUMNS - 1);
    float rowStep = (float)(imageSize-1) / (float)(ROWS - 1);
    for (int frm = 0; frm < FRAMES; ++frm)
    {
        //get drawable from layer
        GimpDrawable *layer = gimp_drawable_get(layers[FRAMES - frm - 1]);
        //initialize region for reading
        GimpPixelRgn rgn_in;
        gimp_pixel_rgn_init(&rgn_in, layer, 0, 0, imageSize, imageSize, FALSE, FALSE);

        for (int col = 0; col < COLUMNS; ++col)
        {
            gimp_pixel_rgn_get_col(&rgn_in, collumn, floatToInt(columnStep * col), 0, imageSize);

            for (int row = 0; row < ROWS; ++row)
            {
                for (int rgb = 0; rgb < RGB; ++rgb)
                {
                    vectorFrames[frm][col][row][rgb] = collumn[floatToInt(rowStep * row) * RGB + rgb];
                }
            }
        }
        gimp_drawable_detach(layer);
        gimp_progress_update((gdouble)(frm) / (gdouble)(FRAMES));
        std::cout << frm << std::endl;
    }

    g_free(collumn);
    g_free(num_layers);
    g_free(layers);

    nlohmann::json jsonFrames;
    jsonFrames = vectorFrames;

    auto done = std::chrono::high_resolution_clock::now();

    std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(done - started).count() << std::endl;

    return jsonFrames;
}