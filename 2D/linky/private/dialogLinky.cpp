#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "dialogLinky.h"

TdialogVals::TdialogVals() : fps(22), debug(true), clearBuffer(true), user_id(""), user_secret(""), installationPath("")
{
    if (fileExists(installationPath + "dialog.json") == false)
    {
        save();
    }
}

void TdialogVals::save()
{
    nlohmann::json dialog;

    dialog["fps"] = fps;
    dialog["debug"] = debug;
    dialog["clearBuffer"] = clearBuffer;
    dialog["user_id"] = user_id;
    dialog["user_secret"] = user_secret;

    std::ofstream outputFileStream(installationPath + "dialog.json");
    outputFileStream << dialog.dump();
    outputFileStream.close();
}

void TdialogVals::load()
{
    std::ifstream inputFileStream(installationPath + "dialog.json");
    nlohmann::json dialog = nlohmann::json::parse(inputFileStream);
    inputFileStream.close();

    fps = dialog["fps"];
    debug = dialog["debug"];
    clearBuffer = dialog["clearBuffer"];
    user_id = dialog["user_id"];
    user_secret = dialog["user_secret"];
}

bool TdialogVals::fileExists(const std::string &name)
{
    if (FILE *file = fopen(name.c_str(), "r"))
    {
        fclose(file);
        return true;
    }
    else
    {
        return false;
    }
}

gboolean dialog(TdialogVals *dialogVals)
{
    dialogVals->load();

    GtkWidget *dialog;    //dialog (window?)
    GtkWidget *main_vbox; //vertical box
    GtkWidget *main_hbox; //horizontal box
    GtkWidget *frame;     //pretty frame with label
    GtkWidget *alignment;
    GtkWidget *fpsLabel; //label for button?
    GtkWidget *fpsSpinbutton;
    GtkObject *fpsSpinbuttonAdjustments;
    GtkWidget *debugCheckButton;
    GtkWidget *clearBufferCheckButton;
    GtkWidget *frameLabel;
    GtkWidget *user_idLabel;
    GtkWidget *user_id;
    GtkWidget *user_secretLabel;
    GtkWidget *user_secret;
    gboolean response;

    gimp_ui_init("linky", FALSE);

    dialog = gimp_dialog_new("Export to Linky CVUT", "linky",
                             NULL, GTK_DIALOG_MODAL,
                             NULL, "linky",
                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                             GTK_STOCK_OK, GTK_RESPONSE_OK,
                             NULL);

    //vertical box
    main_vbox = gtk_vbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), main_vbox);
    gtk_widget_show(main_vbox);

    //frame
    frame = gtk_frame_new(NULL);
    gtk_widget_show(frame);
    gtk_box_pack_start(GTK_BOX(main_vbox), frame, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(frame), 6);

    //alingnment
    alignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(alignment);
    gtk_container_add(GTK_CONTAINER(frame), alignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(alignment), 6, 6, 6, 6);

    //horizontal box
    main_hbox = gtk_hbox_new(FALSE, 38);
    gtk_container_add(GTK_CONTAINER(alignment), main_hbox);
    gtk_widget_show(main_hbox);

    //user_id
    user_idLabel = gtk_label_new_with_mnemonic("User ID:");
    gtk_widget_show(user_idLabel);
    gtk_box_pack_start(GTK_BOX(main_vbox), user_idLabel, FALSE, FALSE, 2);
    gtk_label_set_justify(GTK_LABEL(user_idLabel), GTK_JUSTIFY_LEFT);
    user_id = gtk_entry_new();
    gtk_entry_set_width_chars((GtkEntry *)user_id, 2);
    gtk_widget_show(user_id);
    gtk_box_pack_start(GTK_BOX(main_vbox), user_id, FALSE, FALSE, 2);
    gtk_entry_set_text((GtkEntry *)user_id, dialogVals->user_id.c_str());

    //user_secret
    user_secretLabel = gtk_label_new_with_mnemonic("User secret:");
    gtk_widget_show(user_secretLabel);
    gtk_box_pack_start(GTK_BOX(main_vbox), user_secretLabel, FALSE, FALSE, 2);
    gtk_label_set_justify(GTK_LABEL(user_secretLabel), GTK_JUSTIFY_LEFT);
    user_secret = gtk_entry_new();
    gtk_widget_show(user_secret);
    gtk_box_pack_start(GTK_BOX(main_vbox), user_secret, FALSE, FALSE, 2);
    gtk_entry_set_text((GtkEntry *)user_secret, dialogVals->user_secret.c_str());

    //FPS spin button
    fpsLabel = gtk_label_new_with_mnemonic("FPS:");
    gtk_widget_show(fpsLabel);
    gtk_box_pack_start(GTK_BOX(main_hbox), fpsLabel, FALSE, FALSE, 6);
    gtk_label_set_justify(GTK_LABEL(fpsLabel), GTK_JUSTIFY_RIGHT);
    fpsSpinbuttonAdjustments = gtk_adjustment_new(dialogVals->fps, 10, 44, 1, 5, 5); //default value, min value, max value
    fpsSpinbutton = gtk_spin_button_new(GTK_ADJUSTMENT(fpsSpinbuttonAdjustments), 1, 0);
    gtk_widget_show(fpsSpinbutton);
    gtk_box_pack_start(GTK_BOX(main_hbox), fpsSpinbutton, FALSE, FALSE, 6);
    gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(fpsSpinbutton), TRUE);

    //debug check button
    debugCheckButton = gtk_check_button_new_with_label("Debug mode");
    gtk_box_pack_start(GTK_BOX(main_hbox), debugCheckButton, FALSE, FALSE, 5);
    gtk_widget_show(debugCheckButton);

    //clear buffer button
    clearBufferCheckButton = gtk_check_button_new_with_label("Don't wait for the current animation to end");
    gtk_box_pack_start(GTK_BOX(main_vbox), clearBufferCheckButton, FALSE, FALSE, 5);
    gtk_widget_show(clearBufferCheckButton);

    //frame
    frameLabel = gtk_label_new("Export settings");
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(frame), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);

    //get data from buttons
    g_signal_connect(fpsSpinbuttonAdjustments, "value_changed",
                     G_CALLBACK(gimp_int_adjustment_update),
                     &dialogVals->fps);
    g_signal_connect(debugCheckButton, "toggled",
                     G_CALLBACK(gimp_toggle_button_update),
                     &dialogVals->debug);
    g_signal_connect(clearBufferCheckButton, "toggled",
                     G_CALLBACK(gimp_toggle_button_update),
                     &dialogVals->clearBuffer);

    gtk_widget_show(dialog);

    response = (gimp_dialog_run(GIMP_DIALOG(dialog)) == GTK_RESPONSE_OK);

    dialogVals->user_id = gtk_entry_get_text((GtkEntry *)user_id);
    dialogVals->user_secret = gtk_entry_get_text((GtkEntry *)user_secret);

    gtk_widget_destroy(dialog);

    dialogVals->save();

    return response;
}
