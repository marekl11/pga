#include <libgimp/gimp.h>

#include "curlLinky.h"
#include "framesLinky.h"
#include "dialogLinky.h"

static void query(void);
static void run(const gchar *name, gint nparams, const GimpParam *param, gint *nreturn_vals, GimpParam **return_vals);
GimpPlugInInfo PLUG_IN_INFO = {NULL /*init*/, NULL /*quit*/, query, run};
MAIN()

static void query(void)
{
    static GimpParamDef args[] = {
        {GIMP_PDB_INT32,
         (gchar *)"run-mode",
         (gchar *)"Run mode"},
        {GIMP_PDB_IMAGE,
         (gchar *)"image",
         (gchar *)"Input image"},
        {GIMP_PDB_DRAWABLE,
         (gchar *)"drawable",
         (gchar *)"Input drawable"}};

    gimp_install_procedure(
        "linky",                                                 //name
        "Sends layers (bottom-up) as animation frames to Linky", //blurp
        "Help: Jiri.Chludil@fit.cvut.cz",                        //help
        "Lukas Marek",                                           //author
        "Copyright Lukas Marek",                                 //copyright
        "2021",                                                  //date
        "Export to Linky CVUT",                                  //menu label
        "RGB*",                                                  //image types
        GIMP_PLUGIN,                                             //type
        G_N_ELEMENTS(args),
        NULL, //n_return_vals
        args,
        reinterpret_cast<const GimpParamDef *>(NULL));

    gimp_plugin_menu_register("linky", "<Image>/File/Export"); //name, plugin menu path (might change)
}

static void run(const gchar *name, gint nparams, const GimpParam *param, gint *nreturn_vals, GimpParam **return_vals)
{
    //black magic
    static GimpParam values[1];
    GimpPDBStatusType status = GIMP_PDB_SUCCESS;
    *nreturn_vals = 1;
    *return_vals = values;
    values[0].type = GIMP_PDB_STATUS;
    values[0].data.d_status = status;
    //end of black magic

    //check run mode
    if (param[0].data.d_int32 != GIMP_RUN_INTERACTIVE)
    {
        std::cout << "Only interactive mode is supported!\nAborting" << std::endl;
    }

    std::cout << "Starting..." << std::endl;

    //get drawable
    GimpDrawable *drawable;
    drawable = gimp_drawable_get(param[2].data.d_drawable);

    //check aspect ratio
    if (drawable->width != drawable->height)
    {
        g_message("Aspect ratio is not 1:1!\n           Aborting");
        gimp_drawable_detach(drawable);
        return;
    }
    if (drawable->height < 204)
    {
        g_message("Image is too small!\n          Aborting");
        gimp_drawable_detach(drawable);
        return;
    }

    TdialogVals dialogVals;

    if (dialog(&dialogVals) == false)
    {
        gimp_drawable_detach(drawable);
        return;
    }

    //send data to linky
    if (curlLinkyPlay(curlLinkyTokenCreate(dialogVals.user_id, dialogVals.user_secret, dialogVals.debug, dialogVals.fps), jsonFrames(drawable), dialogVals.clearBuffer) == false)
    {
        g_message("Export failed!\n");
    }
    else
    {
        g_message("Export succsessful!\n");
    }

    gimp_drawable_detach(drawable);
}