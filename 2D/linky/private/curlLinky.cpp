#include <libgimp/gimp.h>
#include <iostream>
#include <string>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

#include "curlLinky.h"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  ((std::string *)userp)->append((char *)contents, size * nmemb);
  return size * nmemb;
}

std::string curlLinkyTokenCreate(const std::string &user_id, const std::string &user_secret, bool debug, int fps)
{
  gimp_progress_init("Creating Json of user info...");

  nlohmann::json inputJson;
  inputJson["user_id"] = user_id;
  inputJson["user_secret"] = user_secret;
  inputJson["debug"] = debug;
  inputJson["fps"] = fps;

  std::string inputString = inputJson.dump();

  gimp_progress_update(0.25);
  std::cout << "Initializing connection with Linky..." << std::endl;
  gimp_progress_set_text("Initializing connection with Linky...");
  CURL *curl = curl_easy_init();
  CURLcode res;
  std::string outputString;
  curl_easy_setopt(curl, CURLOPT_URL, "https://linky.fel.cvut.cz/Api/v1/Token/Create");
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, inputString.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &outputString);
  gimp_progress_update(0.5);
  std::cout << "Retrieving token..." << std::endl;
  gimp_progress_set_text("Retrieving token...");
  curl_easy_perform(curl);
  curl_easy_cleanup(curl);

  gimp_progress_update(0.75);
  std::cout << "\n"
            << outputString << "\n"
            << std::endl;
  nlohmann::json json = nlohmann::json::parse(outputString);

  std::string accesToken = json.at("access_token");
  gimp_progress_update(0.99);
  gimp_progress_set_text("Token received!");
  std::cout << "Token received! " << accesToken << std::endl;

  return accesToken;
}

bool curlLinkyPlay(const std::string &access_token, const nlohmann::json &frames, bool clear_buffer)
{
  gimp_progress_init("Creating Json of data to be sent...");

  nlohmann::json inputJson;
  inputJson["access_token"] = access_token;
  inputJson["frames"] = frames;
  inputJson["clear_buffer"] = clear_buffer;

  std::string inputString = inputJson.dump();

  gimp_progress_update(0.33);
  std::cout << "Initializing connection with Linky..." << std::endl;
  gimp_progress_set_text("Initializing connection with Linky...");
  CURL *curl = curl_easy_init();
  CURLcode res;
  std::string outputString;
  curl_easy_setopt(curl, CURLOPT_URL, "https://linky.fel.cvut.cz/Api/v1/Play");
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, inputString.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &outputString);
  gimp_progress_update(0.66);
  std::cout << "Sending frames..." << std::endl;
  gimp_progress_set_text("Sending frames...");
  curl_easy_perform(curl);
  curl_easy_cleanup(curl);

  gimp_progress_update(0.99);
  std::cout << "Frames sent!" << std::endl;
  gimp_progress_set_text("Frames sent!");
  std::cout << outputString << std::endl;

  //look for "error" in the first 50 characters
  if (outputString.substr(0, 50).find("error") != std::string::npos)
  {
    return false;
  }

  return true;
}